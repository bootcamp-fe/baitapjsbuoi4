// Exercise 1

var ex1HanderlerBtn = document.getElementById("ex1-handler-btn");

function ex1Handler(){
    var ex1Input1 = document.querySelector("#ex1-input #ex-input-1").value*1;
    var ex1Input2 = document.querySelector("#ex1-input #ex-input-2").value*1;
    var ex1Input3 = document.querySelector("#ex1-input #ex-input-3").value*1;
    var st1,st2,st3 ;
    
    if (ex1Input1 > ex1Input2 && ex1Input1 > ex1Input3) {
        st1 = ex1Input1;
        if(ex1Input2 > ex1Input3){
            st2 = ex1Input2;
            st3 = ex1Input3;
        } else {
            st2 = ex1Input3;
            st3 = ex1Input2;
        }
    } else if (ex1Input2 > ex1Input1 && ex1Input2 > ex1Input3){
        st1 = ex1Input2;
        if(ex1Input1 > ex1Input3){
            st2 = ex1Input1;
            st3 = ex1Input3;
        } else {
            st2 = ex1Input3;
            st3 = ex1Input1;
        }
    } else {
        st1 = ex1Input3;
        if(ex1Input1 > ex1Input2){
            st2 = ex1Input1;
            st3 = ex1Input2;
        } else {
            st2 = ex1Input2;
            st3 = ex1Input1;
        }
    }

    var resultEx1 = st3 + "<" + st2 + "<" + st1;


    document.querySelector('#ex1-result #ex-result-text').innerHTML = resultEx1;
}

// Exercise 2

var ex2HanderlerBtn = document.getElementById("ex2-handler-btn");

function ex2Handler(){
    var ex2Input1 = document.querySelector("#ex2-input #ex-input-1");
    var ex2TextResult = ex2Input1.options[ex2Input1.selectedIndex].text;
    if(ex2TextResult != "Chọn thành viên"){
       var resultEx2 =  "Xin chào " + ex2TextResult;
        document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;
    } else {
        var resultEx2 =  "Hãy chọn một ai đó để gửi lời chào !";
        document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;
    }
}

// Exercise 3

var ex3HanderlerBtn = document.getElementById("ex3-handler-btn");

function ex3Handler(){
    var ex3Input1 = document.querySelector("#ex3-input #ex-input-1").value;
    var ex3Input2 = document.querySelector("#ex3-input #ex-input-2").value;
    var ex3Input3 = document.querySelector("#ex3-input #ex-input-3").value;
    var oddNums = 0;
    var evenNums =0;
    var resultEx3;
    if(ex3Input1!="" && ex3Input2!="" &&ex3Input3!=""){
        if(ex3Input1%2!=0){
            oddNums++;
        } 
        if(ex3Input2%2!=0){
            oddNums++;
        }
        if(ex3Input3%2!=0){
            oddNums++;
        }
        evenNums = 3 - oddNums;
        resultEx3 = "Có " + evenNums + " số chẵn, " + oddNums + " số lẻ";
    } else {
        resultEx3 = "Không được để trống ô input!"
    }
    
    document.querySelector('#ex3-result #ex-result-text').innerHTML = resultEx3;
}

// Exercise 4

var ex4HanderlerBtn = document.getElementById("ex4-handler-btn");

function ex4Handler(){
    var ex4Input1 = document.querySelector("#ex4-input #ex-input-1").value*1;
    var ex4Input2 = document.querySelector("#ex4-input #ex-input-2").value*1;
    var ex4Input3 = document.querySelector("#ex4-input #ex-input-3").value*1;
    var canh1 = ex4Input1;
    var canh2 = ex4Input2;
    var canh3 = ex4Input3;
    var resultEx4;
    if(Math.abs(canh2 - canh3)<canh1 && canh1 < (canh2 + canh3) && canh1*canh2*canh3 > 0){
        if(canh1 == canh2 && canh1 == canh3 && canh2 == canh3 ){
            resultEx4 = "Đây là một tam giác đều !";
        } else if(canh1 == canh2 || canh1 == canh3 || canh2 == canh3){
            if((canh1*canh1 + canh2*canh2) == canh3*canh3 || (canh1*canh1 + canh3*canh3) == canh2*canh2 || (canh2*canh2 + canh3*canh3) == canh1*canh1){
                resultEx4 = "Đây là tam giác vuông cân !";
            } else {
                resultEx4 = "Đây là tam giác cân !";
            }
        } else if((canh1*canh1 + canh2*canh2) == canh3*canh3 || (canh1*canh1 + canh3*canh3)==canh2*canh2 || (canh2*canh2 + canh3*canh3) == canh1*canh1){
            resultEx4 = "Đây là tam giác vuông !"
        } else {
            resultEx4 = "Không phải là một tam giác đặc biệt !";
        }
    } else {
        resultEx4 = "Không tồn tại tam giác trên !";
    }

    document.querySelector('#ex4-result #ex-result-text').innerHTML = resultEx4;
}